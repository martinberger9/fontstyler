FROM node:6.10.3-alpine
COPY . /
WORKDIR /
EXPOSE 80
RUN ["apk", "update"]
RUN ["apk", "add", "git", "tar", "bzip2", "wget", "ca-certificates"]
RUN src/checkout 1501321918
CMD ["node", "app.js"]
